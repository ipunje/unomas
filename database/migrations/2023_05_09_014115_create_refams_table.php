<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tbl_uno_mas_refams');
        Schema::create('tbl_uno_mas_refams', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('encargado_id')->nullable()->comment('Usuario en cargado de la refam');
            $table->unsignedInteger('estado_id')->nullable()->comment('Estado en el que se encuentra la refam');
            $table->unsignedInteger('usuario_creador_id')->comment('Usuario creador del registro');
            $table->string('nombre_familia_encargada')->nullable()->comment('familia encargada de la refam');
            $table->string('direccion')->nullable()->comment('Ubicacion de la refam');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('encargado_id')->references('id')->on('users');
            $table->foreign('estado_id')->references('id')->on('tbl_uno_mas_sub_tipos');
            $table->foreign('usuario_creador_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_uno_mas_refams');
    }
};
