<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tbl_uno_mas_asistentes');
        Schema::create('tbl_uno_mas_asistentes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_completo')->nullable()->comment('Nombre de la persona que asiste');
            $table->string('email')->nullable()->comment('email de la persona que asiste');
            $table->string('telefono')->nullable()->comment('Telefono numero celular de la persona');
            $table->dateTime('fecha_nacimiento')->nullable()->comment('fecha nacimiento');
            $table->boolean('is_bautizado')->nullable()->comment('Indica si la persona es bautizada');
            $table->boolean('is_sellado')->nullable()->comment('Indica si la persona es sellada con el espiritu santo');
            $table->unsignedInteger('tipo_id')->nullable()->comment('Hace referencia al tipo de culto que se realiza');
            $table->unsignedInteger('estado_id')->nullable()->comment('Estado del regisgtro');
            $table->unsignedInteger('estado_espiritual_id')->nullable()->comment('Estado espiritual de la persona');
            $table->unsignedInteger('usuario_creador_id')->nullable()->comment('Usuario creador del registro');
            $table->unsignedInteger('refam_principal_id')->nullable()->comment('Id de la refam a la que usualmente va el asistente');
            $table->text('observacion')->nullable()->comment('Observaciones');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('tipo_id')->references('id')->on('tbl_uno_mas_sub_tipos');
            $table->foreign('estado_id')->references('id')->on('tbl_uno_mas_sub_tipos');
            $table->foreign('estado_espiritual_id')->references('id')->on('tbl_uno_mas_sub_tipos');
            $table->foreign('refam_principal_id')->references('id')->on('tbl_uno_mas_sub_tipos');
            $table->foreign('usuario_creador_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_uno_mas_asistentes');
    }
};
