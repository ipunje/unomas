<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tbl_uno_mas_asistencias');
        Schema::create('tbl_uno_mas_asistencias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('eventoable_id');
            $table->string('eventoable_type');
            $table->integer('asistencia_id')->comment('Id de la persona que asiste al evento');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_uno_mas_asistencias');
    }
};
