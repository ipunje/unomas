<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tbl_uno_mas_cultos');
        Schema::create('tbl_uno_mas_cultos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tipo_id')->nullable()->comment('Hace referencia al tipo de culto que se realiza');
            $table->unsignedInteger('usuario_creador_id')->nullable()->comment('Usuario creador del registro');
            $table->integer('ofrenda')->nullable()->comment('Ofrenda colectada en el servicio');
            $table->string('nombre_predicador')->nullable()->comment('Nombre de la persona quien predica');
            $table->string('lema')->nullable()->comment('Lema del culto');
            $table->text('observacion')->nullable()->comment('Observaciones');
            $table->integer('asistencia_total')->nullable()->comment('Asistencia total del culto');
            $table->integer('asistencia_ninos')->nullable()->comment('Asistencia total del culto ninos');
            $table->integer('asistencia_simpatizantes')->nullable()->comment('Asistencia total de simpatizante');
            $table->integer('asistencia_amigos')->nullable()->comment('Asistencia total de amigos');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('tipo_id')->references('id')->on('tbl_uno_mas_sub_tipos');
            $table->foreign('usuario_creador_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_uno_mas_cultos');
    }
};
