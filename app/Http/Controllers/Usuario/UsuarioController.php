<?php

namespace App\Http\Controllers\Usuario;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UsuarioController extends Controller
{
    public function vue()
    {
        return view('vue');
    }

    public function index() 
    {
        dd(Auth::user());
    }

}