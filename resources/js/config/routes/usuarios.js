import usuarios from '_app/usuarios/index.vue';

const routes = [
    { 
        path: '/usuarios',
        component: usuarios
    },
  ];

export default routes;

